import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Calculator',
      theme: ThemeData(
        primarySwatch: Colors.red,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int firstNum;
  int secoundNum;
  String output = "";
  String res;
  String opt;

  void btnclicked(String btnVal) {
    if (btnVal == "C") {
      output = "";
      firstNum = 0;
      secoundNum = 0;
      res = "";
    } else if (btnVal == "+" ||
        btnVal == "-" ||
        btnVal == "*" ||
        btnVal == "/") {
      firstNum = int.parse(output);
      res = "";
      opt = btnVal;
    } else if (btnVal == "=") {
      secoundNum = int.parse(output);
      if (opt == "+") {
        res = (firstNum + secoundNum).toString();
      }
      if (opt == "-") {
        res = (firstNum - secoundNum).toString();
      }
      if (opt == "*") {
        res = (firstNum * secoundNum).toString();
      }
      if (opt == "/") {
        res = (firstNum / secoundNum).toString();
      }
    } else {
      res = int.parse(output + btnVal).toString();
    }
    setState(() {
      output = res;
    });
  }

  Widget customButton(String btnVal) {
    return Expanded(
        child: OutlineButton(
      padding: EdgeInsets.all(25.0),
      onPressed: () => btnclicked(btnVal),
      child: Text(
        "$btnVal",
        style: TextStyle(fontSize: 25.0),
      ),
    ));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Calculator"),
      ),
      body: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            Expanded(
              child: Container(
                padding: EdgeInsets.all(10.0),
                alignment: Alignment.bottomRight,
                child: Text(
                  "$output",
                  style: TextStyle(fontSize: 30, fontWeight: FontWeight.w600),
                ),
              ),
            ),
            Row(
              children: <Widget>[
                customButton("9"),
                customButton("8"),
                customButton("7"),
                customButton("+")
              ],
            ),
            Row(
              children: <Widget>[
                customButton("6"),
                customButton("5"),
                customButton("4"),
                customButton("-")
              ],
            ),
            Row(
              children: <Widget>[
                customButton("3"),
                customButton("2"),
                customButton("1"),
                customButton("*")
              ],
            ),
            Row(
              children: <Widget>[
                customButton("C"),
                customButton("0"),
                customButton("="),
                customButton("/")
              ],
            )
          ],
        ),
      ),
    );
  }
}
